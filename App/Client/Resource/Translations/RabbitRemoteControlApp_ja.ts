<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>CFavoriteView</name>
    <message>
        <location filename="../../FavoriteView.cpp" line="220"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="221"/>
        <source>Open settings and connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="224"/>
        <source>New group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="226"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="260"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="260"/>
        <source>Input group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="265"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="265"/>
        <source>The group [%1] is existed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CFrmFullScreenToolBar</name>
    <message>
        <location filename="../../FrmFullScreenToolBar.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="34"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="38"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="39"/>
        <source>Nail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="42"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="43"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="44"/>
        <source>Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="48"/>
        <source>Zoom to windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="51"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="54"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="66"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="70"/>
        <source>ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="72"/>
        <source>Add to favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="74"/>
        <source>TabBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="77"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="78"/>
        <source>Tab bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="80"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="81"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="82"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="85"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="86"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="87"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CParameterDlgSettings</name>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="14"/>
        <location filename="../../ParameterDlgSettings.ui" line="55"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="66"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="212"/>
        <source>Shot screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="239"/>
        <source>No action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="255"/>
        <source>Open folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="268"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="94"/>
        <source>Recent open file max count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="314"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="85"/>
        <source>Favorite: select it then double node edit, other connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="111"/>
        <source>Main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="205"/>
        <source>Main window receiver short cut key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="301"/>
        <source>Save main window status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="137"/>
        <source>Tab position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="149"/>
        <source>North</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="175"/>
        <source>South</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="162"/>
        <source>West</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="188"/>
        <source>East</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="279"/>
        <source>Save Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="289"/>
        <source>Brower(&amp;B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="308"/>
        <source>System tray icon context menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="321"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="328"/>
        <source>Recent open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="335"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="224"/>
        <source>Select shot remote desktop, otherwise shot window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="198"/>
        <source>Resume the connections when it was last closed at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="117"/>
        <source>Enable system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="39"/>
        <source>Ok(&amp;O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="46"/>
        <source>No(&amp;N)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.cpp" line="160"/>
        <source>Open shot screen path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Rabbit Remote Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="32"/>
        <source>Help(&amp;H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="39"/>
        <source>View(&amp;V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="43"/>
        <source>Sink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="54"/>
        <location filename="../../mainwindow.cpp" line="147"/>
        <location filename="../../mainwindow.cpp" line="148"/>
        <location filename="../../mainwindow.cpp" line="149"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="82"/>
        <source>Remote(&amp;R)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="86"/>
        <source>Connect(&amp;C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="127"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="155"/>
        <source>About(&amp;A)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="158"/>
        <location filename="../../mainwindow.ui" line="161"/>
        <location filename="../../mainwindow.ui" line="164"/>
        <location filename="../../mainwindow.ui" line="167"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="175"/>
        <location filename="../../mainwindow.ui" line="178"/>
        <location filename="../../mainwindow.ui" line="181"/>
        <location filename="../../mainwindow.ui" line="184"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="198"/>
        <location filename="../../mainwindow.ui" line="201"/>
        <location filename="../../mainwindow.ui" line="204"/>
        <location filename="../../mainwindow.ui" line="207"/>
        <source>ToolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="246"/>
        <location filename="../../mainwindow.ui" line="249"/>
        <location filename="../../mainwindow.ui" line="252"/>
        <location filename="../../mainwindow.ui" line="255"/>
        <source>Original size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="291"/>
        <source>Keep aspect ration zoom to window(&amp;K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="294"/>
        <location filename="../../mainwindow.ui" line="297"/>
        <location filename="../../mainwindow.ui" line="300"/>
        <location filename="../../mainwindow.ui" line="303"/>
        <source>Keep aspect ration zoom to window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="318"/>
        <location filename="../../mainwindow.ui" line="321"/>
        <location filename="../../mainwindow.ui" line="324"/>
        <location filename="../../mainwindow.ui" line="327"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="354"/>
        <location filename="../../mainwindow.ui" line="357"/>
        <location filename="../../mainwindow.ui" line="360"/>
        <location filename="../../mainwindow.ui" line="363"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="375"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="172"/>
        <source>Update(&amp;U)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="95"/>
        <source>Open Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="195"/>
        <source>ToolBar(&amp;T)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="216"/>
        <location filename="../../mainwindow.cpp" line="329"/>
        <source>Full screen(&amp;F)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="219"/>
        <location filename="../../mainwindow.ui" line="222"/>
        <location filename="../../mainwindow.ui" line="225"/>
        <location filename="../../mainwindow.ui" line="228"/>
        <location filename="../../mainwindow.cpp" line="330"/>
        <location filename="../../mainwindow.cpp" line="331"/>
        <location filename="../../mainwindow.cpp" line="332"/>
        <source>Full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="243"/>
        <source>Original size(&amp;O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="267"/>
        <source>Zoom to window(&amp;Z)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="270"/>
        <location filename="../../mainwindow.ui" line="273"/>
        <location filename="../../mainwindow.ui" line="276"/>
        <location filename="../../mainwindow.ui" line="279"/>
        <source>Zoom to window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="315"/>
        <source>Exit(&amp;E)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="336"/>
        <location filename="../../mainwindow.ui" line="339"/>
        <location filename="../../mainwindow.ui" line="342"/>
        <source>Recently connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="351"/>
        <source>Disconnect(&amp;D)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="372"/>
        <source>Open(&amp;O) rabbit remote control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="406"/>
        <source>Open(&amp;O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="389"/>
        <source>Default(&amp;D)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="392"/>
        <location filename="../../mainwindow.ui" line="395"/>
        <location filename="../../mainwindow.ui" line="398"/>
        <location filename="../../mainwindow.ui" line="401"/>
        <source>Default style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="409"/>
        <location filename="../../mainwindow.ui" line="412"/>
        <location filename="../../mainwindow.ui" line="415"/>
        <location filename="../../mainwindow.ui" line="418"/>
        <source>Open style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="423"/>
        <source>Send Ctl+Alt+Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="438"/>
        <location filename="../../mainwindow.ui" line="441"/>
        <source>Show TabBar(&amp;B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="444"/>
        <location filename="../../mainwindow.ui" line="447"/>
        <location filename="../../mainwindow.ui" line="450"/>
        <source>Show TabBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="462"/>
        <location filename="../../mainwindow.ui" line="465"/>
        <location filename="../../mainwindow.ui" line="468"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="480"/>
        <location filename="../../mainwindow.ui" line="483"/>
        <location filename="../../mainwindow.ui" line="486"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="510"/>
        <location filename="../../mainwindow.ui" line="513"/>
        <location filename="../../mainwindow.ui" line="516"/>
        <location filename="../../mainwindow.ui" line="519"/>
        <source>Screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="528"/>
        <location filename="../../mainwindow.ui" line="531"/>
        <location filename="../../mainwindow.ui" line="534"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="543"/>
        <location filename="../../mainwindow.ui" line="546"/>
        <location filename="../../mainwindow.ui" line="549"/>
        <location filename="../../mainwindow.ui" line="552"/>
        <source>Current connect parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="561"/>
        <location filename="../../mainwindow.ui" line="564"/>
        <location filename="../../mainwindow.ui" line="567"/>
        <source>Clone current connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="579"/>
        <location filename="../../mainwindow.ui" line="582"/>
        <location filename="../../mainwindow.ui" line="585"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="594"/>
        <location filename="../../mainwindow.ui" line="597"/>
        <location filename="../../mainwindow.ui" line="600"/>
        <location filename="../../mainwindow.ui" line="603"/>
        <source>Add to favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="608"/>
        <location filename="../../mainwindow.ui" line="611"/>
        <location filename="../../mainwindow.ui" line="614"/>
        <source>Open log directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="619"/>
        <location filename="../../mainwindow.ui" line="622"/>
        <location filename="../../mainwindow.ui" line="625"/>
        <source>Open log file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="630"/>
        <location filename="../../mainwindow.ui" line="633"/>
        <location filename="../../mainwindow.ui" line="636"/>
        <source>Open log configure file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="495"/>
        <location filename="../../mainwindow.ui" line="498"/>
        <location filename="../../mainwindow.ui" line="501"/>
        <source>Zoom window to remote desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="137"/>
        <location filename="../../mainwindow.cpp" line="138"/>
        <location filename="../../mainwindow.cpp" line="139"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="77"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="214"/>
        <source>ICE singal status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="357"/>
        <source>Exit full screen(&amp;E)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="358"/>
        <location filename="../../mainwindow.cpp" line="359"/>
        <location filename="../../mainwindow.cpp" line="360"/>
        <source>Exit full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="569"/>
        <location filename="../../mainwindow.cpp" line="587"/>
        <source>Load file fail: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="378"/>
        <location filename="../../mainwindow.ui" line="381"/>
        <location filename="../../mainwindow.ui" line="384"/>
        <location filename="../../mainwindow.cpp" line="579"/>
        <source>Open rabbit remote control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="581"/>
        <source>Rabbit remote control Files (*.rrc);;All files(*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="669"/>
        <source>Connecting to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="695"/>
        <source>Connected to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="722"/>
        <source>ICE singal status: Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="731"/>
        <source>ICE singal status: Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="912"/>
        <source>Save screenslot to </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="69"/>
        <source>Rabbit Remote Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="70"/>
        <source>Kang Lin Studio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
