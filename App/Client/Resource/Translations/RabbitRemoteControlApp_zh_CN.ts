<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CFavoriteView</name>
    <message>
        <location filename="../../FavoriteView.cpp" line="220"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="221"/>
        <source>Open settings and connect</source>
        <translation>打开设置再连接</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="224"/>
        <source>New group</source>
        <translation>新建组</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="226"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="260"/>
        <source>Input</source>
        <translation>输入</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="260"/>
        <source>Input group name</source>
        <translation>输入组名</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="265"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="265"/>
        <source>The group [%1] is existed</source>
        <translation>组 [%1] 已经存在</translation>
    </message>
</context>
<context>
    <name>CFrmFullScreenToolBar</name>
    <message>
        <location filename="../../FrmFullScreenToolBar.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="34"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="38"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="39"/>
        <source>Nail</source>
        <translation>钉住</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="42"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="43"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="44"/>
        <source>Full</source>
        <translation>关闭全屏</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="48"/>
        <source>Zoom to windows</source>
        <translation>远程桌面缩放到客户端窗口大小</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="51"/>
        <source>Origin</source>
        <translation>远程桌面还原到原始大小</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="54"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="66"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="70"/>
        <source>ScreenShot</source>
        <translation>截屏</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="72"/>
        <source>Add to favorite</source>
        <translation>增加到收藏夹</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="85"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="86"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="87"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="80"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="81"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="82"/>
        <source>Disconnect</source>
        <translation>关闭连接</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="74"/>
        <source>TabBar</source>
        <translation>标签条</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="77"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="78"/>
        <source>Tab bar</source>
        <translation>标签条</translation>
    </message>
</context>
<context>
    <name>CParameterDlgSettings</name>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="14"/>
        <location filename="../../ParameterDlgSettings.ui" line="55"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="66"/>
        <source>General</source>
        <translation>普通</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="212"/>
        <source>Shot screen</source>
        <translation>截屏</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="239"/>
        <source>No action</source>
        <translation>无动作</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="255"/>
        <source>Open folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="268"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="94"/>
        <source>Recent open file max count:</source>
        <translation>最近打开文件最大数：</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="314"/>
        <source>No</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="85"/>
        <source>Favorite: select it then double node edit, other connect</source>
        <translation>收藏夹：选择它，双击编辑节点；否则连接</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="111"/>
        <source>Main window</source>
        <translation>主窗口</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="205"/>
        <source>Main window receiver short cut key</source>
        <translation>主窗口接收快捷键</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="301"/>
        <source>Save main window status</source>
        <translation>保存主窗口状态</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="137"/>
        <source>Tab position</source>
        <translation>标签位置</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="149"/>
        <source>North</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="175"/>
        <source>South</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="162"/>
        <source>West</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="188"/>
        <source>East</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="279"/>
        <source>Save Path:</source>
        <translation>保存文件夹：</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="289"/>
        <source>Brower(&amp;B)</source>
        <translation>浏览(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="308"/>
        <source>System tray icon context menu</source>
        <translation>系统托盘图标右键菜单</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="321"/>
        <source>Remote</source>
        <translation>远程菜单</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="328"/>
        <source>Recent open</source>
        <translation>最近打开菜单</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="335"/>
        <source>Favorite</source>
        <translation>收藏夹</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="224"/>
        <source>Select shot remote desktop, otherwise shot window</source>
        <translation>选择截取远程桌面，否则截取窗口</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="198"/>
        <source>Resume the connections when it was last closed at startup</source>
        <translation>启动时，恢复上次关闭时的连接</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="117"/>
        <source>Enable system tray icon</source>
        <translation>允许系统托盘图标</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="39"/>
        <source>Ok(&amp;O)</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="46"/>
        <source>No(&amp;N)</source>
        <translation>取消(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.cpp" line="160"/>
        <source>Open shot screen path</source>
        <translation>打开截屏文件夹</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Rabbit Remote Control</source>
        <translation>玉兔远程控制</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="32"/>
        <source>Help(&amp;H)</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="39"/>
        <source>View(&amp;V)</source>
        <translation>视图(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="54"/>
        <location filename="../../mainwindow.cpp" line="147"/>
        <location filename="../../mainwindow.cpp" line="148"/>
        <location filename="../../mainwindow.cpp" line="149"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="82"/>
        <source>Remote(&amp;R)</source>
        <translation>远程(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="95"/>
        <source>Open Log</source>
        <translation>打开日志</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="127"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="155"/>
        <source>About(&amp;A)</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="172"/>
        <source>Update(&amp;U)</source>
        <translation>更新(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="195"/>
        <source>ToolBar(&amp;T)</source>
        <translation>工具条(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="216"/>
        <location filename="../../mainwindow.cpp" line="329"/>
        <source>Full screen(&amp;F)</source>
        <translation>全屏(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="267"/>
        <source>Zoom to window(&amp;Z)</source>
        <translation>远程桌面缩放到窗口大小(&amp;Z)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="270"/>
        <location filename="../../mainwindow.ui" line="273"/>
        <location filename="../../mainwindow.ui" line="276"/>
        <location filename="../../mainwindow.ui" line="279"/>
        <source>Zoom to window</source>
        <translation>远程桌面缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="318"/>
        <location filename="../../mainwindow.ui" line="321"/>
        <location filename="../../mainwindow.ui" line="324"/>
        <location filename="../../mainwindow.ui" line="327"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="354"/>
        <location filename="../../mainwindow.ui" line="357"/>
        <location filename="../../mainwindow.ui" line="360"/>
        <location filename="../../mainwindow.ui" line="363"/>
        <source>Disconnect</source>
        <translation>关闭边连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="375"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="392"/>
        <location filename="../../mainwindow.ui" line="395"/>
        <location filename="../../mainwindow.ui" line="398"/>
        <location filename="../../mainwindow.ui" line="401"/>
        <source>Default style</source>
        <translation>默认样式</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="409"/>
        <location filename="../../mainwindow.ui" line="412"/>
        <location filename="../../mainwindow.ui" line="415"/>
        <location filename="../../mainwindow.ui" line="418"/>
        <source>Open style</source>
        <translation>打开样式</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="444"/>
        <location filename="../../mainwindow.ui" line="447"/>
        <location filename="../../mainwindow.ui" line="450"/>
        <source>Show TabBar</source>
        <translation>显示标签条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="462"/>
        <location filename="../../mainwindow.ui" line="465"/>
        <location filename="../../mainwindow.ui" line="468"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="480"/>
        <location filename="../../mainwindow.ui" line="483"/>
        <location filename="../../mainwindow.ui" line="486"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="510"/>
        <location filename="../../mainwindow.ui" line="513"/>
        <location filename="../../mainwindow.ui" line="516"/>
        <location filename="../../mainwindow.ui" line="519"/>
        <source>Screenshot</source>
        <translation>截屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="528"/>
        <location filename="../../mainwindow.ui" line="531"/>
        <location filename="../../mainwindow.ui" line="534"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="543"/>
        <location filename="../../mainwindow.ui" line="546"/>
        <location filename="../../mainwindow.ui" line="549"/>
        <location filename="../../mainwindow.ui" line="552"/>
        <source>Current connect parameters</source>
        <translation>当前连接参数</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="561"/>
        <location filename="../../mainwindow.ui" line="564"/>
        <location filename="../../mainwindow.ui" line="567"/>
        <source>Clone current connect</source>
        <translation>克隆当前连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="579"/>
        <location filename="../../mainwindow.ui" line="582"/>
        <location filename="../../mainwindow.ui" line="585"/>
        <source>Favorites</source>
        <translation>收藏夹</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="594"/>
        <location filename="../../mainwindow.ui" line="597"/>
        <location filename="../../mainwindow.ui" line="600"/>
        <location filename="../../mainwindow.ui" line="603"/>
        <source>Add to favorite</source>
        <translation>增加到收藏夹</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="608"/>
        <location filename="../../mainwindow.ui" line="611"/>
        <location filename="../../mainwindow.ui" line="614"/>
        <source>Open log directory</source>
        <translation>打开日志文件目录</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="619"/>
        <location filename="../../mainwindow.ui" line="622"/>
        <location filename="../../mainwindow.ui" line="625"/>
        <source>Open log file</source>
        <translation>打开日志文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="630"/>
        <location filename="../../mainwindow.ui" line="633"/>
        <location filename="../../mainwindow.ui" line="636"/>
        <source>Open log configure file</source>
        <translation>打开日志配置文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="495"/>
        <location filename="../../mainwindow.ui" line="498"/>
        <location filename="../../mainwindow.ui" line="501"/>
        <source>Zoom window to remote desktop</source>
        <translation>缩放窗口到远程桌面大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="158"/>
        <location filename="../../mainwindow.ui" line="161"/>
        <location filename="../../mainwindow.ui" line="164"/>
        <location filename="../../mainwindow.ui" line="167"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="175"/>
        <location filename="../../mainwindow.ui" line="178"/>
        <location filename="../../mainwindow.ui" line="181"/>
        <location filename="../../mainwindow.ui" line="184"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="198"/>
        <location filename="../../mainwindow.ui" line="201"/>
        <location filename="../../mainwindow.ui" line="204"/>
        <location filename="../../mainwindow.ui" line="207"/>
        <source>ToolBar</source>
        <translation>工具条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="243"/>
        <source>Original size(&amp;O)</source>
        <translation>远程桌面还原到原始大小(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="246"/>
        <location filename="../../mainwindow.ui" line="249"/>
        <location filename="../../mainwindow.ui" line="252"/>
        <location filename="../../mainwindow.ui" line="255"/>
        <source>Original size</source>
        <translation>原始大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="291"/>
        <source>Keep aspect ration zoom to window(&amp;K)</source>
        <translation>远程桌面保持纵横比缩放到窗口大小(&amp;K)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="294"/>
        <location filename="../../mainwindow.ui" line="297"/>
        <location filename="../../mainwindow.ui" line="300"/>
        <location filename="../../mainwindow.ui" line="303"/>
        <source>Keep aspect ration zoom to window</source>
        <translation>远程桌面保持纵横比缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="315"/>
        <source>Exit(&amp;E)</source>
        <translation>退出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="372"/>
        <source>Open(&amp;O) rabbit remote control file</source>
        <translation>打开玉兔远程控制文件(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="423"/>
        <source>Send Ctl+Alt+Del</source>
        <translation>发送 Ctl+Alt+Del</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="438"/>
        <location filename="../../mainwindow.ui" line="441"/>
        <source>Show TabBar(&amp;B)</source>
        <translation>显示标签条(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="86"/>
        <source>Connect(&amp;C)</source>
        <translation>连接(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="336"/>
        <location filename="../../mainwindow.ui" line="339"/>
        <location filename="../../mainwindow.ui" line="342"/>
        <source>Recently connected</source>
        <translation>最近连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="351"/>
        <source>Disconnect(&amp;D)</source>
        <translation>断开连接(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="406"/>
        <source>Open(&amp;O)</source>
        <translation>打开(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="389"/>
        <source>Default(&amp;D)</source>
        <translation>默认(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="43"/>
        <source>Sink</source>
        <translation>换肤</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="137"/>
        <location filename="../../mainwindow.cpp" line="138"/>
        <location filename="../../mainwindow.cpp" line="139"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="219"/>
        <location filename="../../mainwindow.ui" line="222"/>
        <location filename="../../mainwindow.ui" line="225"/>
        <location filename="../../mainwindow.ui" line="228"/>
        <location filename="../../mainwindow.cpp" line="330"/>
        <location filename="../../mainwindow.cpp" line="331"/>
        <location filename="../../mainwindow.cpp" line="332"/>
        <source>Full screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="77"/>
        <source>Favorite</source>
        <translation>收藏夹</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="214"/>
        <source>ICE singal status</source>
        <translation>ICE 信令状态</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="357"/>
        <source>Exit full screen(&amp;E)</source>
        <translation>退出全屏(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="358"/>
        <location filename="../../mainwindow.cpp" line="359"/>
        <location filename="../../mainwindow.cpp" line="360"/>
        <source>Exit full screen</source>
        <translation>退出全屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="569"/>
        <location filename="../../mainwindow.cpp" line="587"/>
        <source>Load file fail: </source>
        <translation>加载文件失败：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="378"/>
        <location filename="../../mainwindow.ui" line="381"/>
        <location filename="../../mainwindow.ui" line="384"/>
        <location filename="../../mainwindow.cpp" line="579"/>
        <source>Open rabbit remote control file</source>
        <translation>打开玉兔远程控制文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="581"/>
        <source>Rabbit remote control Files (*.rrc);;All files(*.*)</source>
        <translation>玉兔远程控制文件(*.rrc);;所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="669"/>
        <source>Connecting to </source>
        <translation>正在连接 </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="695"/>
        <source>Connected to </source>
        <translation>连接到 </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="722"/>
        <source>ICE singal status: Connected</source>
        <translation>ICE 信令状态：连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="731"/>
        <source>ICE singal status: Disconnected</source>
        <translation>ICE 信令状态：断开</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="912"/>
        <source>Save screenslot to </source>
        <translation>保存截屏到 </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="69"/>
        <source>Rabbit Remote Control</source>
        <translation>玉兔远程控制</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="70"/>
        <source>Kang Lin Studio</source>
        <translation>康林工作室</translation>
    </message>
</context>
</TS>
