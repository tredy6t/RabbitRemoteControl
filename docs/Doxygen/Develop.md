
## Rabbit Remote Control Development Document

Author: Kang Lin kl222@126.com

### Project location

+ Main repository: https://github.com/KangLin/RabbitRemoteControl
+ Mirror repository:
  - sourceforge: https://sourceforge.net/projects/rabbitremotecontrol/
  - gitlab: https://gitlab.com/kl222/RabbitRemoteControl
  - launchpad: https://launchpad.net/rabbitremotecontrol
  - gitee: https://gitee.com/kl222/RabbitRemoteControl
    
### Semantic Versioning:

- Semantic Versioning: [https://semver.org/](https://semver.org/)

### Compile

- [Linux](../Compile/Linux.md)
- [Windows](../Compile/Windows.md)

### Module

- [Module](modules.html)
