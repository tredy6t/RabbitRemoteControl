<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>CFrmParameterICE</name>
    <message>
        <source>ICE</source>
        <translation></translation>
    </message>
    <message>
        <source>Enable ICE</source>
        <translation>允許 ICE</translation>
    </message>
    <message>
        <source>Enable ICE debug</source>
        <translation>允許 ICE 調試</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <source>Signal server:</source>
        <translation>信令服務器：</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>端口</translation>
    </message>
    <message>
        <source>Signal user:</source>
        <translation>信令用戶：</translation>
    </message>
    <message>
        <source>Format: user@domain/Resource</source>
        <translation>格式：user@domain/Resource</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <source>Stun server:</source>
        <translation>STUN 服務器：</translation>
    </message>
    <message>
        <source>Turn server:</source>
        <translation>TURN 服務器：</translation>
    </message>
    <message>
        <source>Turn user:</source>
        <translation>TURN 用戶：</translation>
    </message>
    <message>
        <source>The user name format is error. please use format: user@domain/resource</source>
        <translation>用戶格式錯誤。請用格式：用戶名@域名/資源</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>斷開連接</translation>
    </message>
</context>
</TS>
