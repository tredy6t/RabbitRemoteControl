<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CFrmParameterICE</name>
    <message>
        <source>ICE</source>
        <translation></translation>
    </message>
    <message>
        <source>Enable ICE</source>
        <translation>允许 ICE</translation>
    </message>
    <message>
        <source>Enable ICE debug</source>
        <translation>允许 ICE 调试</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Signal server:</source>
        <translation>信令服务器：</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>端口</translation>
    </message>
    <message>
        <source>Signal user:</source>
        <translation>信令用户：</translation>
    </message>
    <message>
        <source>Format: user@domain/Resource</source>
        <translation>格式：user@domain/Resource</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <source>Stun server:</source>
        <translation>STUN 服务器：</translation>
    </message>
    <message>
        <source>Turn server:</source>
        <translation>TURN 服务器：</translation>
    </message>
    <message>
        <source>Turn user:</source>
        <translation>TURN 用户：</translation>
    </message>
    <message>
        <source>The user name format is error. please use format: user@domain/resource</source>
        <translation>用户格式错误。请用格式：用户名@域名/资源</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>断开连接</translation>
    </message>
</context>
</TS>
